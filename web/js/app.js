$(document).ready(function() {

 // search-form-landing submission
 $('#search_generate').on('click', function (e) {

   e.preventDefault();

   let submit_button = $(e.currentTarget);
   let form = submit_button.closest('form');
   let url = form.attr('action');

   $.ajax({
     url: url,
     method: 'POST',
     data: form.serialize(),
     success: function(response) {
       if(response.status == 'valid') {
         $('#resume-container').html(response.template);
       }
       else if (response.status == 'invalid') {
         alert('invalid')
       }
     }
   });

 })

});

// spinner
let loading = $('#spinner-container').hide();

$(document)
  .ajaxStart(function () {
    loading.show();
  })
  .ajaxStop(function () {
    loading.hide();
  });