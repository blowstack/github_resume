<?php


namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;



class Search {

  /**
   * @Assert\NotBlank
   * @Assert\Type("string")
   * @var string
   */
  public $name;

}