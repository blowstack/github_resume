<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Search;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\SearchType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Utils\GitHubApi;


class LandingPageController extends Controller
{

  /**
   * @param Request $request
   * @param GitHubApi $gitHubApi
   * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
   * @Route("/", name="landing_page")
   */
  public function indexAction(Request $request, GitHubApi $gitHubApi)
  {

    $search = new Search();

    $form = $this->get('form.factory')->createNamed('search', SearchType::class, $search);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid() && $request->isXmlHttpRequest()) {

      $name = $form->get('name')->getData();

      $userData = $gitHubApi->getUserInfo($name, $gitHubApi::PRIVACY_PUBLIC, $gitHubApi::AFFILIATION_OWNER);
      $languages = $gitHubApi->evaluateUserLanguages($userData);

      return new JsonResponse([
        'status' => 'valid',
        'template' => $this->renderView('landing_page/_resume.html.twig', [
          'userData' => $userData,
          'languages' => $languages
        ])
      ]);
    }
    elseif ($form->isSubmitted() && $request->isXmlHttpRequest()) {
      return new JsonResponse([
        'status' => 'invalid'
      ]);

    }

    return $this->render('landing_page/index.html.twig', [
      'form' =>  $form->createView(),
    ]);
  }
}
