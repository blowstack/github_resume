<?php


namespace AppBundle\Utils;

interface GitHubApiInterface {

  const AFFILIATION_OWNER = 'OWNER';
  const AFFILIATION_COLLABORATOR = 'COLLABORATOR';
  const AFFILIATION_ORGANIZATION_MEMBER = 'ORGANIZATION_MEMBER';
  const PRIVACY_PUBLIC = 'PUBLIC';
  const PRIVACY_PRIVATE = 'PRIVATE';
  const IS_FORK_TRUE = true;
  const IS_FORK_FALSE = false;
  const IS_FORK_NULL = null;
  const LIMIT_MAX = 100;


  /**
   * @param string $host
   * @param string $token
   * @param string $user_agent
   * @param string $query
   * @param array $variables
   * @return array
   */
  public function sendRequest(string $host, string $token, string $user_agent, string $query, ?array $variables): array ;


  /**
   * @param string $name
   * @param string $privacy
   * @param string $affiliations
   * @param bool $isFork
   * @param int $limit
   * @return array
   */
  public function getUserInfo(string $name, string $privacy, string $affiliations, bool $isFork, int $limit): array;

  /**
   * @param array $userData
   * @return array|null
   */
  public function evaluateUserLanguages(array $userData): ?array;

}