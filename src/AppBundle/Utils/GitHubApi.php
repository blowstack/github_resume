<?php


namespace AppBundle\Utils;

class GitHubApi implements GitHubApiInterface {

  private $github_host;
  private $github_token;
  private $github_user_agent;

  public function __construct($github_host, $github_token, $github_user_agent)
  {
    $this->github_host = $github_host;
    $this->github_token = $github_token;
    $this->github_user_agent = $github_user_agent;
  }


  /**
   * @param string $name
   * @param string $privacy
   * @param string $affiliations
   * @param bool $isFork
   * @param int $limit
   * @return array
   */
  public function getUserInfo(string $name, string $privacy, string $affiliations, ?bool $isFork = self::IS_FORK_NULL, int $limit = self::LIMIT_MAX): array {

    // the limit can't exceed LIMIT_MAX value
    $limit = ($limit > 0 && $limit <= self::LIMIT_MAX) ? $limit : self::LIMIT_MAX;

    $query = <<<'QUERY'
      query GetUser($name: String!, $limit: Int, $privacy: RepositoryPrivacy, $affiliations: [RepositoryAffiliation], $isFork: Boolean) {
        repositoryOwner(login: $name) {
          login ... on User {
          name
          login
          bio
          websiteUrl
          email
          location
          createdAt
          avatarUrl
            repositories(last: $limit, privacy: $privacy, affiliations: $affiliations, ownerAffiliations: $affiliations, isFork: $isFork, orderBy: { field: CREATED_AT, direction: DESC}) {
              edges { 
                node { 
                  name
                  url
                  description
                  createdAt
              }
              node { 
                primaryLanguage {
                  name
                }
              }
            }
            totalCount
            }
            followers {
              totalCount
            }
        }
      }
    }
QUERY;

    $variables = [
      'name' => $name,
      'privacy' => $privacy,
      'affiliations' => $affiliations,
      'isFork' => $isFork,
      'limit' => $limit,
    ];

    return $this->sendRequest($this->github_host, $this->github_token, $this->github_user_agent, $query, $variables);
  }


  /**
   * @param $host
   * @param $token
   * @param $user_agent
   * @param $query
   * @param $variables
   * @return array
   */
  public function sendRequest(string $host, string $token, string $user_agent, string $query, ?array $variables): array {

    $curl = curl_init($host);
    $body = json_encode(['query' => $query, 'variables' => $variables ]);

    curl_setopt($curl, CURLOPT_TIMEOUT, 5);
    curl_setopt($curl, CURLOPT_VERBOSE, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
      ["Authorization: {$token}", "User-Agent: {$user_agent}"]);

    $result = curl_exec($curl);
    curl_close($curl);

    return json_decode($result, true);
  }

  public function evaluateUserLanguages(array $userData): ?array {

    $repositories = $userData['data']['repositoryOwner']['repositories']['edges'];
    $languages = [];
    $total = 0;

    if ($repositories == null ) {
      return null;
    }

    foreach ($repositories as $item ) {

      if (isset($item['node']['primaryLanguage'])) {

        $lang = $item['node']['primaryLanguage']['name'];
        $languages[$lang] = array_key_exists($lang, $languages) ? $languages[$lang] + 1 : 1 ;
        $total++;
      }
    }

    foreach ($languages as &$lang) {
      $lang = round($lang * 100 / $total);
    }

    arsort($languages);

    return $languages;
  }

}